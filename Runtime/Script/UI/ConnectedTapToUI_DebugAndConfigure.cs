﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ConnectedTapToUI_DebugAndConfigure : MonoBehaviour
{
    public InputField m_connectedOrder;
    public InputField m_name;
    public InputField m_id;
    public Image m_isConnectedState;
    public Color m_isDisconnected;
    public Color m_isConnected;
    public Text m_firmware;
    public Text m_gyro;
    public Text m_accelerometer;
    public Text m_mouseVelocity;
    public Image m_mouseActive;
    public InputField m_tapReceiveHistory;
    public Button m_vibrate;
    public Text m_lastReceived;
    public int m_milliSecondPerClick=500;

    public TapToUI_DebugFinger[] m_finger= new TapToUI_DebugFinger[5];
    public Dropdown m_modeType;
    public Slider m_accelerometerSensibility;
    public Slider m_gyroSensibility;
    public Slider m_fingersSensibility;
    public Image m_isMouse;
    public void Awake()
    {
        m_vibrate.onClick.AddListener(Vibrate);
        m_modeType.onValueChanged.AddListener(ChangeMode);
    }

    private void ChangeMode(int arg0)
    {
        if (TapUtility.GetRegisteredCount() <= 0) return;
        int index = 0;
        int.TryParse(m_connectedOrder.text, out index);
        ConnectedTap tap = TapUtility.GetByConnectionOrder(index);
        switch (arg0)
        {
            case 0: tap.SetAsMode(TapMode.Controller); break;
            case 1: tap.SetAsMode(TapMode.Text); break;
            case 2: tap.SetAsMode(TapMode.ControllerAndMouse); break;
            case 3:
                PushSensibilityToTheDevice(tap);
                tap.SetAsMode(TapMode.Sensors); break;
            default:
                break;
        }
    }

    private void PushSensibilityToTheDevice(ConnectedTap tap)
    {
        if (tap == null)
            return;
        tap.SetSensorsSensibility((int)m_accelerometerSensibility.value, (int)m_gyroSensibility.value, (int)m_fingersSensibility.value);
    }

    private void Vibrate()
    {
        if (TapUtility.GetRegisteredCount() <= 0) return;
        int index = 0;
        int.TryParse(m_connectedOrder.text, out index);
        ConnectedTap tap = TapUtility.GetByConnectionOrder(index);
        tap.Vibrate(m_milliSecondPerClick);
    }

    public DateTime lastCheck;
    public void Update()
    {
        DateTime now = DateTime.Now ;

        try
        {
            string v3Format = "{0}\t:\t{1}\t:\t{2}";
            Vector3 tmpVector = Vector3.zero;
            int index = 0;
            int.TryParse(m_connectedOrder.text, out index);
            int numberOfDevice = TapUtility.GetRegisteredCount();
            ConnectedTap tap = numberOfDevice <= 0 ? new ConnectedTap() :
                TapUtility.GetByConnectionOrder(index);

            m_name.text = tap.GetName();
            m_id.text = tap.GetIdentifiant();
            m_isConnectedState.color = tap.IsConnected() ? m_isConnected : m_isDisconnected;
            m_firmware.text = "" + tap.GetFirmwareId();
            tmpVector = tap.GetGyro();
            m_gyro.text = "Gyro " + string.Format(v3Format, tmpVector.x, tmpVector.y, tmpVector.z);
            tmpVector = tap.GetAccelerometer();
            m_accelerometer.text = "Acc. " + string.Format(v3Format, tmpVector.x, tmpVector.y, tmpVector.z);
            tmpVector = tap.GetMouseVelocity();
            m_mouseVelocity.text = "Mouse " + string.Format(v3Format, tmpVector.x, tmpVector.y, tmpVector.z);
            m_mouseActive.color = tap.GetTimeSinceLastMouseMove()<1.5f ? (tap.IsMouseLaserSensorWasActive()? Color.green:Color.yellow) : Color.white;
            m_isMouse.color = tap.IsMouseLaserSensorWasActive() ? Color.green : Color.white;
            m_tapReceiveHistory.text = tap.GetHistoryAsMultiLineText('o', '.');
            for (int i = 0; i < m_finger.Length; i++)
            {
                Finger f = (Finger)i;
                if (m_finger[i] != null)
                {
                    m_finger[i].SetRawData(tap.GetFingerAccelerometer(f));
                    m_finger[i].SetOn(tap.WasTap(f));
                    m_finger[i].SetName(f.ToString());

                }
            }
            m_lastReceived.text = tap.GetLastTapReceived().GetDescription();
        }
        catch (Exception e) {
            m_tapReceiveHistory.text = e.StackTrace;
        }

        //FINGER
        // History
        lastCheck = now;

    }


    private void OnValidate()
    {

        if (m_finger.Length > 5)
        {
            m_finger = m_finger.Take(5).ToArray();
        }
    }
}
