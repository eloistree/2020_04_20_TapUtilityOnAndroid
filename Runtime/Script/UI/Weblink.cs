﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weblink : MonoBehaviour
{
    public string m_url;
    public void Open()
    {
        Application.OpenURL(m_url);

    }
    public void OpenSpecificURL(string url)
    {
        Application.OpenURL(url);

    }
}
