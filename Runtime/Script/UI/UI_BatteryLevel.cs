﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BatteryLevel : MonoBehaviour
{
    public Image m_imageLevel;
    public Text m_textDisplay;
    public BatteryLevelState m_appStart = new BatteryLevelState(0, DateTime.Now);
    public BatteryLevelState m_now = new BatteryLevelState(0, DateTime.Now);

    public class BatteryLevelState {
        public float level;
        public DateTime recordedTime;

        public BatteryLevelState(float v, DateTime now)
        {
            this.level = v;
            this.recordedTime = now;
        }

        public static float GetPourcentPerHour(BatteryLevelState start, BatteryLevelState now)
        {
            double timePast = (now.recordedTime - start.recordedTime).TotalSeconds;
            double batteryLost = (start.level - now.level);
            return (float)((batteryLost / timePast) * 3600.0);


        }
    }

    void Start()
    {
        InvokeRepeating("CheckBatteryLevel", 0, 2);
        m_appStart = new BatteryLevelState(GetBatteryLevel(), DateTime.Now);
    }
    public void CheckBatteryLevel() {
        float level = GetBatteryLevel();
        float pourcentPerHour = 0;

        m_now = new BatteryLevelState(GetBatteryLevel(), DateTime.Now);
        pourcentPerHour = BatteryLevelState.GetPourcentPerHour(m_appStart, m_now);
        m_imageLevel.fillAmount = level;
     //   m_textDisplay.text = string.Format("{0:0.0}\n%/h", level, pourcentPerHour);

        m_textDisplay.text = string.Format("{0:0.0}% - {1:0.0}%/h", level, pourcentPerHour);

    }
    public static float GetBatteryLevel()
    {
#if UNITY_IOS
         UIDevice device = UIDevice.CurrentDevice();
         device.batteryMonitoringEnabled = true; // need to enable this first
         Debug.Log("Battery state: " + device.batteryState);
         Debug.Log("Battery level: " + device.batteryLevel);
         return device.batteryLevel*100;
#elif UNITY_ANDROID

        if (Application.platform == RuntimePlatform.Android)
        {
            try
            {
                using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
                {
                    if (null != unityPlayer)
                    {
                        using (AndroidJavaObject currActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                        {
                            if (null != currActivity)
                            {
                                using (AndroidJavaObject intentFilter = new AndroidJavaObject("android.content.IntentFilter", new object[] { "android.intent.action.BATTERY_CHANGED" }))
                                {
                                    using (AndroidJavaObject batteryIntent = currActivity.Call<AndroidJavaObject>("registerReceiver", new object[] { null, intentFilter }))
                                    {
                                        int level = batteryIntent.Call<int>("getIntExtra", new object[] { "level", -1 });
                                        int scale = batteryIntent.Call<int>("getIntExtra", new object[] { "scale", -1 });

                                        // Error checking that probably isn't needed but I added just in case.
                                        if (level == -1 || scale == -1)
                                        {
                                            return 50f;
                                        }
                                        return ((float)level / (float)scale) * 100.0f;
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception)
            {

            }
        }

        return 100;
#endif
        return 1;
    }
}
