﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TapToUI_DebugFinger : MonoBehaviour
{
    public Text m_name;
    public Image m_isFingerOn;
    public Color m_isOn;
    public Color m_isOff;
    public Text m_rawAcceleromterData;

    public void SetName(string name) {
        m_name.text = name; 
    }

    public void SetOn(bool value) {
        m_isFingerOn.color = value ? m_isOn : m_isOff;
    }
    public void SetRawData(Vector3 data) {
        m_rawAcceleromterData.text = string.Format("{0}:{1}:{2}", data.x, data.y, data.z);
    }

}
