﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI2TapMapper : MonoBehaviour
{
    public TapMappingRegister<string> m_register= new TapMappingRegister<string>();
   
    public InputField m_mappingName;
    public Dropdown m_mappingRegisteredList;
    public Toggle[] m_fingersState;
    public InputField m_textToStore;

    public UnityEvent m_onHadChanged;

    public void SetRegister(TapMappingRegister<string> register)
    {
        m_register = register;
        RefreshValue();
        m_onHadChanged.Invoke();
      

    }

    public TapMappingRegister<string> GetRegister() { return m_register; }


    

    void Awake()
    {
   
        TapMapping<string> def = new TapMapping<string>("Default");
        m_register.Add(def);
        foreach (var item in TapCombo.GetAllPossibilities())
        {
            def.Set(item, "> "+item.GetIdDescription('O','.'));
        }
        m_mappingRegisteredList.onValueChanged.AddListener(SetTheMappingToWorkOn);
        
        StartListening();
    }

    private void SetTheMappingToWorkOn(int arg0)
    {
        m_mappingName.text = m_mappingRegisteredList.options[arg0].text;
    }

    private void OnDestroy()
    {
        StopListening();
    }

    private void StartListening()
    {
        m_textToStore.onValueChanged.AddListener(SetOrAddValue);
        m_mappingName.onValueChanged.AddListener(RefreshValue);
        for (int i = 0; i < m_fingersState.Length; i++)
        {
            m_fingersState[i].onValueChanged.AddListener(RefreshValue);
        }
    }

    private void StopListening()
    {
        m_textToStore.onValueChanged.RemoveListener(SetOrAddValue);
        m_mappingName.onValueChanged.RemoveListener(RefreshValue);
        for (int i = 0; i < m_fingersState.Length; i++)
        {
            m_fingersState[i].onValueChanged.RemoveListener(RefreshValue);
        }
    }


    private void RefreshValue(bool arg0)
    {
        RefreshValue();
    }
    private void RefreshValue(int arg0)
    {
        RefreshValue();
    }
    private void RefreshValue(string arg0)
    {
        RefreshValue();
    }
    private void RefreshValue()
    {

        StopListening();
        bool[] fingersSelected = GetFingersSelected();
        string mapName = m_mappingName.text;
        string text = m_textToStore.text;
        TapCombo value = new TapCombo(fingersSelected);
        TapMapping<string> map = m_register.GetMapping(mapName);
      
        if (map!=null && map.IsValueRegistered(value))
            m_textToStore.text = map.GetValue(value);
        else m_textToStore.text = "";

        RefreshMapsRegisteredToDropbox();
        StartListening();
    }

    private void SetOrAddValue(string arg0)
    {
        bool[] fingersSelected = GetFingersSelected();
        string mapName = m_mappingName.text;
        string text = m_textToStore.text;
        TapCombo value = new TapCombo( fingersSelected);
        if(!m_register.IsRegistered(mapName))
            m_register.AddNew(mapName);
        TapMapping<string> map = m_register.GetMapping(mapName);
        map.Set(value, text);
        m_onHadChanged.Invoke();
    }

    private bool[] GetFingersSelected()
    {
        bool[] value = new bool[5];
        try
        {
            for (int i = 0; i < 5; i++)
            {
                value[i] = m_fingersState[i].isOn;
            }

        }
        catch (NullReferenceException) {
            Debug.LogError("My code allow only to have exactly 5 troggles to work.");
        }
        return value;
    }


    private void RefreshMapsRegisteredToDropbox()
    {
        List<string> mapNames = m_register.GetMappingRegistered();
        m_mappingRegisteredList.ClearOptions();
        m_mappingRegisteredList.AddOptions(mapNames);
    }


}
