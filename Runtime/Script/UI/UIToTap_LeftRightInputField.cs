﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIToTap_LeftRightInputField : MonoBehaviour
{
    public InputField m_left;
    public InputField m_right;
  
    private void Start()
    {
        TapUtility.LoadHandsIdFromDataFolder();
        StopListeningListeningInput();
        if (m_left != null)
            m_left.text = TapUtility.GetStoredLeftHandId();
        if (m_right != null)
            m_right.text = TapUtility.GetStoredRightHandId();

        StartListeningInput();
    }

    private void OnDestroy()
    {
        StopListeningListeningInput();
    }

    private void ChangeRightHandID(string value)
    {
        TapUtility.SetRightHand(value);
        TapUtility.SaveHandsIdToDataFolder();
    }

    private void ChangeLeftHandID(string value)
    {
        TapUtility.SetLeftHand(value);
        TapUtility.SaveHandsIdToDataFolder();
    }
    private void OnHandsIDChanged(string left, string right)
    {
        StopListeningListeningInput();
        if (m_left != null)
            m_left.text = left;
        if (m_right != null)
            m_right.text = right;
        StartListeningInput();
    }

    public void SwitchLeftRight()
    {
        TapUtility.SwitchLeftAndRight();
        TapUtility.SaveHandsIdToDataFolder();

    }
    public void StartListeningInput()
    {
        if (m_left != null)
            m_left.onValueChanged.AddListener(ChangeLeftHandID);
        if (m_right != null)
            m_right.onValueChanged.AddListener(ChangeRightHandID);
        TapUtility.AddListenerOnLeftRightHandChanged(OnHandsIDChanged);
    }
    public void StopListeningListeningInput()
    {
        if (m_left != null)
            m_left.onValueChanged.RemoveListener(ChangeLeftHandID);
        if (m_right != null)
            m_right.onValueChanged.RemoveListener(ChangeRightHandID);
        TapUtility.RemoveListenerOnLeftRightHandChanged(OnHandsIDChanged);
    }
}
