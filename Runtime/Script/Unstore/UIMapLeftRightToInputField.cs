﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIMapLeftRightToInputField : MonoBehaviour
{

    public UI2TapMapper m_mapper;
    public Dropdown m_leftHand;
    public Dropdown m_rightHand;
    public InputField m_whereToWrite;

    public DateTime m_previousCheck;
    public DateTime m_now;

    public TapMapperReceivedEvent        m_onTapReceivedTranslated;
    public TapMapperTranslatedUnityEvent m_onTranslationFound;

    void Start()
    {
        m_mapper.m_onHadChanged.AddListener(RefreshMaps);
 
    }



    private string GetMapperInfoFor(HandSide hand, TapCombo tap)
    {
        if (hand == HandSide.Unknown) return "";
        Dropdown dBox = null;
        if (hand == HandSide.Left)
            dBox = m_leftHand;
        if (hand == HandSide.Right)
            dBox = m_rightHand;

        if (dBox.options.Count <= 0) return "";
        string mapName = dBox.options[dBox.value].text;
            
        if (m_mapper == null || !m_mapper.GetRegister().IsRegistered(mapName))
            return null;
        return m_mapper.GetRegister().GetMapping(mapName).GetValue(tap);


    }

    public void RefreshMaps()
    {
        int index;
        List<string> list = m_mapper.m_register.GetMappingRegistered();
        index = m_leftHand.value;
        m_leftHand.ClearOptions();
        m_leftHand.AddOptions(list);
        m_leftHand.value = index;
        index = m_rightHand.value;
        m_rightHand.ClearOptions();
        m_rightHand.AddOptions(list);
        m_rightHand.value = index;
    }

    void Update()
    {
        DateTime now = m_now = DateTime.Now;
        TapReceived tapLeft = GetLastTapIfSome(HandSide.Left, m_previousCheck, now);
        TapReceived tapRight = GetLastTapIfSome(HandSide.Right, m_previousCheck, now);

        if (tapLeft != null || tapRight != null) {

                
                if (tapLeft != null) {

                   string info = GetMapperInfoFor(HandSide.Left, tapLeft.GetValue());
                    if (m_whereToWrite != null)
                        m_whereToWrite.text = tapLeft.GetDescription()+": "+info + "\n" + m_whereToWrite.text;
                     if(!string.IsNullOrEmpty(info) && m_onTapReceivedTranslated!=null)
                            m_onTapReceivedTranslated(tapLeft,info);
                if (!string.IsNullOrEmpty(info))
                    m_onTranslationFound.Invoke(info);
            }
                if (tapRight != null)
                {
                    string info = GetMapperInfoFor(HandSide.Right, tapRight.GetValue());
                    if(m_whereToWrite!=null)
                        m_whereToWrite.text = tapRight.GetDescription() + ": " + info + "\n" + m_whereToWrite.text;
                    if (!string.IsNullOrEmpty(info) && m_onTapReceivedTranslated != null)
                        m_onTapReceivedTranslated(tapRight, info);
                    if(!string.IsNullOrEmpty(info))
                    m_onTranslationFound.Invoke(info);
            }
                if (m_whereToWrite != null && m_whereToWrite.text.Length > 2000)
                    m_whereToWrite.text = m_whereToWrite.text.Substring(0, 2000);
            
        }

        m_previousCheck = now;
    }


    //private bool IsMouseMove(HandSide hand)
    //{
    //    ConnectedTap tap = null;
    //    if (hand == HandSide.Left ) tap = TapUtility.GetLeft();
    //    if (hand == HandSide.Right) tap = TapUtility.GetRight();
        
    //    if (tap != null)
    //    {

    //        return tap.WasMouseModeActive(m_previousCheck, m_now);

    //    }
    //    return false;
    //}

    private TapReceived GetLastTapIfSome(HandSide hand, DateTime previous, DateTime now)
    {
        ConnectedTap tap = null;
        if (hand == HandSide.Left)tap= TapUtility.GetLeft();
        if (hand == HandSide.Right)tap= TapUtility.GetRight();

        if (tap != null)
        {
            TapReceived last;
            TapReceived[] lasts;

            if (tap.WasTapSince(previous, now, out last, out lasts))
            {
                return last;

            }

        }
        return null;

    }
}

public delegate void TapMapperReceivedEvent   (TapReceived received, string translation);
[System.Serializable]
public class         TapMapperTranslatedUnityEvent : UnityEvent<string> { }
