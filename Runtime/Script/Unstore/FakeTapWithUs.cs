﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeTapWithUs : MonoBehaviour
{
#if UNITY_EDITOR
    public string identifier = "Sim:Azerty:Sim";

    public KeyCode[] m_fingers = new KeyCode[5] {
    KeyCode.A,
    KeyCode.Z,
        KeyCode.E,
            KeyCode.R,
                KeyCode.T
    };
    public KeyCode m_validate = KeyCode.Space;
    public KeyCode m_mouseMode = KeyCode.V;
    public float m_sensibility=2f;
    [Header("Debug")]
    public string m_fingerState="";
    public ConnectedTap m_fakeTap = new ConnectedTap();
    private void Awake()
    {
        TapUtility.CheckRegistered(identifier);
        m_fakeTap = TapUtility.GetById(identifier);
        m_fakeTap.SetInfo(identifier, name, -1);
        m_fakeTap.SetConnected(true);
        TapUtility.SetRightHand(identifier);
    }

    void Update()
    {
        bool[] state = new bool[5];
        state[0] = Input.GetKey(m_fingers[0]) ;
        state[1] = Input.GetKey(m_fingers[1]) ;
        state[2] = Input.GetKey(m_fingers[2]) ;
        state[3] = Input.GetKey(m_fingers[3]) ;
        state[4] = Input.GetKey(m_fingers[4]) ;
        char[] val = new char[5];
        val[0] = Input.GetKey(m_fingers[0]) ? 'O' : '.';
        val[1] = Input.GetKey(m_fingers[1]) ? 'O' : '.';
        val[2] = Input.GetKey(m_fingers[2]) ? 'O' : '.';
        val[3] = Input.GetKey(m_fingers[3]) ? 'O' : '.';
        val[4] = Input.GetKey(m_fingers[4]) ? 'O' : '.';

        if (Input.GetKeyDown(m_validate)) {
            
            m_fingerState = val.ToString();
            TapReceived received = new TapReceived(state);
            m_fakeTap.AddReceived(received);

        }

        //MOUSE TO DELTA SIM
        m_currentPosition = Input.mousePosition;
        if (m_lastMousePosition == Vector3.zero)
            m_lastMousePosition = m_currentPosition;
        m_mouseDelta =  m_currentPosition- m_lastMousePosition;
        //if(Input.GetKey(m_mouseMode))
        if(m_mouseDelta!=Vector3.zero)
        m_fakeTap.SetMouseDelta(m_mouseDelta.x* m_sensibility, m_mouseDelta.y* m_sensibility);
        m_fakeTap.SetMouseMode(Input.GetKey(m_mouseMode));
        m_lastMousePosition = m_currentPosition;
    }

     Vector3 m_lastMousePosition;
     Vector3 m_currentPosition;
     Vector3 m_mouseDelta;
#endif
}
