﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tap2MouseScreenSimulation : MonoBehaviour
{

    public UI_FakeCursorScreen m_screen;
    public Dropdown m_handType;
    public Slider m_resetCursorTime;
    public Slider m_sensibilityMultiplicator;
    public Toggle m_inverseHorizontal;
    public Toggle m_inverseVertical;
    public Toggle m_clamp;
    public Toggle m_useRecenter;
    public bool m_useClamp;
    public bool m_didNotMoveSinceLong;

    void Update()
    {
        HandSide hand = m_handType.value == 0 ? HandSide.Left : HandSide.Right;
        ConnectedTap tap = TapUtility.GetHandOrNull(hand);
        if (tap == null)
            return;

        DateTime now=DateTime.Now;
        MouseDeltaRecord [] moves = tap.GetMouseInfo().GetDelta(previous, now ); 
        if (moves.Length > 0) {
            if (m_useRecenter.isOn && m_didNotMoveSinceLong)
            {
                m_didNotMoveSinceLong = false;
                m_screen.SetCursorPositionWithPourcent(0.5f, 0.5f);
            }
            for (int i = 0; i < moves.Length; i++)
            {
                m_screen.AddPixel(moves[i].GetMoveX()*(m_inverseHorizontal.isOn?-1f:1f)* m_sensibilityMultiplicator.value, moves[i].GetMoveY() * (m_inverseVertical.isOn ? -1f : 1f) * m_sensibilityMultiplicator.value, m_clamp.isOn);

            }
        }
        MouseDeltaRecord rec = tap.GetMouseInfo().GetLastMove();
        float maxTime = m_resetCursorTime.value;
        if (rec !=null && ( now - rec.GetTime() ).TotalSeconds> maxTime ) {
            m_didNotMoveSinceLong = true;
        }

        previous = now;
    }
    DateTime previous;

    public string GetInfoToSaveAsJson() {
        throw new System.NotImplementedException("Need to be code");
    }

    public void SetInfoToSaveFromJson(string json) {
        throw new System.NotImplementedException("Need to be code");
    }

    public Vector2 GetCursorPourcentPosition()
    {
       return  m_screen.GetCursorPourcentPosition();
    }

    public Vector2 GetCursorPixelPosition()
    {
        return m_screen.GetCursorPixelPosition();
    }
}
