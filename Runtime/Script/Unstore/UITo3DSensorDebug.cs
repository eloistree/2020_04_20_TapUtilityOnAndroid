﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITo3DSensorDebug : MonoBehaviour
{
    public ConnectedTap m_selection;

    public InputField m_id;

    public Text m_gyroDisplay;
    public Text m_accelerometerDisplay;
    public Text[] m_fingersAccelerometer;

    public Transform m_gyro;
    public Transform m_accelerometer;
    public Transform[] m_fingersaccelerometer;


    public bool m_autoCheckForSensor;


    void Update()
    {
        if (m_id == null || string.IsNullOrEmpty(m_id.text))
            m_selection = TapUtility.GetByConnectionOrder(0);
        else m_selection = TapUtility.GetById(m_id.text);

        if (m_selection == null) 
            return;
        m_id.text = m_selection.GetIdentifiant();

        m_gyroDisplay.text = VectorToText(m_selection.GetGyro());
        m_accelerometerDisplay.text = VectorToText(m_selection.GetAccelerometer());

        for (int i = 0; i < 5; i++)
        {
            m_fingersAccelerometer[i].text = VectorToText(m_selection.GetFingerAccelerometer(i));
        }


    }
    public string VectorToText(Vector3 accelerometer)
    {
        return string.Format("{0:0.0}:{1:0.0}:{2:0.0}", accelerometer.x, accelerometer.y, accelerometer.z);
    }
}
