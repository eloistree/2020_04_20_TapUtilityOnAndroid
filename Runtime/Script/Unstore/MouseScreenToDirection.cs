﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseScreenToDirection : MonoBehaviour
{
    public UI_FakeCursorScreen m_mousescreen;
    public float m_borderInPourcentHorizontal = 0.3f;
    public float m_borderInPourcentVertical = 0.2f;
    public Toggle m_left;
    public Toggle m_top;
    public Toggle m_right;
    public Toggle m_down;
    public InputField m_lastResult;
    private bool[] m_lastState = new bool[4];
    private bool[] m_currentState = new bool[4];

    void Update()
    {
        Vector2 pct = m_mousescreen.GetCursorPourcentPosition();
        m_left.isOn = m_currentState [0]  = (pct.x < m_borderInPourcentHorizontal);
        m_top.isOn = m_currentState[1]    =(pct.y > 1f - m_borderInPourcentVertical);
        m_right.isOn = m_currentState[2]  =(pct.x > 1f - m_borderInPourcentHorizontal);
        m_down.isOn = m_currentState[3]   =(pct.y < m_borderInPourcentVertical);

        string c = "";
        bool found = false;
        for (int i = 0; i < 4; i++)
        {
            if (m_currentState[i] != m_lastState[i] && m_currentState[i]) {

                c = GetArrowChar(); found = true;
            }
            m_lastState[i] = m_currentState[i];
        }
        if (found)
        {
            m_lastResult.text = c+ " " + m_lastResult.text;

        }
    }

    public void SetBorder(float value) { SetBorder(value, value); }
    public void SetBorder(float horizontalBorder, float verticalBorder) {
        m_borderInPourcentHorizontal = horizontalBorder;
        m_borderInPourcentVertical = verticalBorder;
    }

    public bool IsLeft() { return m_currentState[0]; }
    public bool IsTop() { return m_currentState[1]; }
    public bool IsRight() { return m_currentState[2]; }
    public bool IsDown() { return m_currentState[3]; }
    private string GetArrowChar()
    {
        if (IsTop() && IsLeft()) return "7";  
        if (IsTop() && IsRight()) return "9";
        if (IsDown() && IsLeft()) return "1";
        if (IsDown() && IsRight()) return "3";
        if (IsLeft()) return "←";
        if (IsTop()) return "↑";
        if (IsRight()) return "→";
        if (IsDown()) return "↓";
        return "O";

     
    }

    public string GetLastDirectionAsChar()
    {
        if (m_lastResult!=null && m_lastResult.text.Length > 0)
            return ""+m_lastResult.text[0];
        return "";
    }
}
