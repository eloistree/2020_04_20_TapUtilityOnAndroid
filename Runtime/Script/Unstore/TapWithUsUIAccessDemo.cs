﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TapWithUsUIAccessDemo : MonoBehaviour
{
    [TextArea(0, 15)]
    public string m_howToUse;


    public TapUtilityMono m_tapUtilityMono;
    public UI2TapMapper m_tapMapper;
    public Tap2MouseScreenSimulation m_leftMouseDeltaSimulation;
    public MouseScreenToDirection m_leftMouseDirection;
    public Tap2MouseScreenSimulation m_rightMouseDeltaSimulation;
    public MouseScreenToDirection m_rightMouseDirection;


    public InputField m_tapReceivedDebug;
    public Dropdown m_mapAvailable;
    public InputField m_mouseLeftRightDebug;
    public float m_timeBetweenRefresh=0.1f;

    public void Start()
    {
        InvokeRepeating("Refresh", 0, m_timeBetweenRefresh);
    }

    public void OnEnable()
    {
     
    }

    DateTime m_previousFrameTime;
    public void Refresh()
    {
        int mapIndex=0;
        DateTime frameTimeNow = DateTime.Now;
        TapMappingRegister<string> tapRegisters = m_tapMapper.GetRegister();

        mapIndex= m_mapAvailable.value ;
        m_mapAvailable.ClearOptions();
        m_mapAvailable.AddOptions(tapRegisters.GetMappingRegistered());
        m_mapAvailable.value = mapIndex;
        string mapName = "";
        if ( m_mapAvailable.options.Count>0)
            mapName = m_mapAvailable.options[m_mapAvailable.value].text;
        TapMapping<string> tapSelected = tapRegisters.GetMapping(mapName);


        ConnectedTap tapDevice = null;
        TapReceived tapReceived = TapUtility.GetLastTapReceived(out tapDevice);

        string translation = "";
        if (tapSelected != null && tapReceived!=null)
        {
            if (tapSelected.IsValueRegistered(tapReceived.GetValue()))
                translation = tapSelected.GetValue(tapReceived.GetValue());
        }

            if(m_tapReceivedDebug!=null)
            m_tapReceivedDebug.text = string.Format("Id:{0} Tap({1}): {2}\n Map ({3}):{4}\n",
            tapDevice.GetIdentifiant(),
            tapDevice.GetHandType(),
            tapReceived.GetDescription('O','.'),
            tapSelected == null ? "None" : tapSelected.GetIdName(),
            tapSelected == null ? "None" : translation);//            + m_tapReceivedDebug.text;

        // DO THE MOUSE / DIRECTION DEBUG PART
        Vector2 leftTmp = m_leftMouseDeltaSimulation.GetCursorPourcentPosition()
            , rightTmp = m_rightMouseDeltaSimulation.GetCursorPourcentPosition()
            , leftTmpPx = m_leftMouseDeltaSimulation.GetCursorPixelPosition()
            , rightTmpPx = m_rightMouseDeltaSimulation.GetCursorPixelPosition();
        m_mouseLeftRightDebug.text = (
                string.Format("{0} {1} <L|R> {2} {3}\n",
              m_leftMouseDirection.GetLastDirectionAsChar(), GetV2ToString(leftTmp), GetV2ToString(rightTmp)
              , m_rightMouseDirection.GetLastDirectionAsChar())

              +string.Format("{0} {1}px <L|R> {2}px {3}\n",
              m_leftMouseDirection.GetLastDirectionAsChar(), GetV2ToString(leftTmpPx),
              GetV2ToString(rightTmpPx), m_rightMouseDirection.GetLastDirectionAsChar())

         );//+        m_mouseLeftRightDebug.text;

        if (m_tapReceivedDebug.text.Length > 4000)
            m_tapReceivedDebug.text = m_tapReceivedDebug.text.Substring(0, 4000);
        if (m_mouseLeftRightDebug.text.Length > 4000)
            m_mouseLeftRightDebug.text = m_mouseLeftRightDebug.text.Substring(0, 4000);


        m_previousFrameTime = frameTimeNow;
    }

    private string GetV2ToString(Vector2 vector)
    {
        return string.Format("({0:0.0},{1:0.0})", vector.x, vector.y);
    }

    void Awake() {
        StartListening();
    }

    private void StartListening()
    {

    }

    void OnDestroy() {
        StopListening();
    }

    private void StopListening()
    {

    }

    private void OnValidate()
    {
        m_howToUse = "Hello.\nUse TapUtility to communicutate with Tap With Us devices.\n" +
        "Go in TapWithUsOfficialAPI if you want the developers officiel code.\n" +
        "https://github.com/TapWithUs/tap-unity-plugin \n" +
        "Or the package manager I did for Unity 2019+ \n" +
        "https://github.com/EloiStree/2020_04_25_TapUnityPluginAsPackage \n" +
        "Some code to facilitate the use of the Tap are found here under this text.\n" +
        "They could be cleaner and not dependant of Unity UI. But I don't have time.\n" +
        "And I don't have gain to do that so...  :) \n" +
        "Have fun.\n" +
        "\n" +
        "Feel free to contact me if needed:\n" +
        "http://eloistree.page.link/talk \n" +
        "\n" +
        "Donation are always welcome:\n" +
        "http://eloistree.page.link/patreon \n" +
        "http://eloistree.page.link/paypal \n";
    }
}
