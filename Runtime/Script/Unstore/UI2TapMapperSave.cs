﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UI2TapMapperSave : MonoBehaviour
{

    public string m_saveName="LeftRigthHand";
    public UI2TapMapper m_saveFrom;
    public TapMappingRegisterAsStringSave m_registerSave;

    public void Awake()
    {

        LoadFromPersistantFile();
    }

    public void OnEnable()
    {
        LoadFromPersistantFile();
        m_saveFrom.m_onHadChanged.AddListener(TapChange);
    }

   

    public void OnDisable()
    {
        m_saveFrom.m_onHadChanged.RemoveListener(TapChange);
        SaveToPeristantFile();
    }
    private void OnApplicationFocus(bool focus)
    {
        if (focus == false)
            SaveToPeristantFile();
    }
    private void TapChange()
    {
        SaveToPeristantFile();
    }

    public void OnValidate()
    {
        m_saveName = m_saveName.Replace(" ", "").Replace(" ", "").Replace(" ", "");
    }

    public void SaveToPeristantFile()
    {
        m_registerSave = TapMappingStringRegisterToJson.GetTapMappingRegisterSave(m_saveFrom.GetRegister());
        string json = TapMappingStringRegisterToJson.GetRegisterAsJSON(m_saveFrom.GetRegister());
        SaveDataInFile(json, m_saveName);
    }
    public void LoadFromPersistantFile() {
        string json =LoadDataFromFile(m_saveName);
        if (json != null)
            m_saveFrom.SetRegister( TapMappingStringRegisterToJson.GetRegisterFromJSON(json, out m_registerSave));
    }


    public static void SaveDataInFile(string text,string fileNameWithoutExtention , bool onharddrive=true)
    {
        UnityDirectoryStorage.SaveFile("TapUtility", fileNameWithoutExtention + ".json", text, onharddrive) ;
    }

    public static string LoadDataFromFile( string fileNameWithoutExtention, bool onharddrive = true)
    {
        return UnityDirectoryStorage.LoadFile("TapUtility", fileNameWithoutExtention + ".json", onharddrive);

    }
}
