﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMapLeftRightToDropdown : MonoBehaviour
{
    public UI2TapMapper m_tapMap;
    public Dropdown m_leftHand;
    public Dropdown m_rightHand;
    void Start()
    {
        m_tapMap.m_onHadChanged.AddListener(RefreshMaps);
    }
    private void OnEnable()
    {
        RefreshMaps();


    }

    public void RefreshMaps()
    {
        int l, r;
        List<string> list = m_tapMap.m_register.GetMappingRegistered();
        l = m_leftHand.value;
        m_leftHand.ClearOptions();
        m_leftHand.AddOptions(list);
        m_leftHand.value = l;

        r = m_rightHand.value;
        m_rightHand.ClearOptions();
        m_rightHand.AddOptions(list);
        m_rightHand.value = r;
    }

    public string GetLeftMapName()
    {
        if (m_leftHand == null || m_leftHand.options.Count < 1)
            return "";
        return m_leftHand.options[m_leftHand.value].text;
    }
    public string GetRightMapName()
    {
        if (m_rightHand == null || m_rightHand.options.Count < 1)
            return "";
        return m_rightHand.options[m_rightHand.value].text;
    }
}
