﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITapDemoPages : MonoBehaviour
{
    public Page[] m_pages;


    [SerializeField] int m_changePageEditor;

    //private void OnValidate()
    //{
    //    SetPage(m_changePageEditor);
    //}

    public void SetAllPagesOff() {
        SetPage(-1);
    }
    public void SetPage(int index) {
        if (m_pages.Length > 0) { 
        
            for (int i = 0; i < m_pages.Length; i++)
            {
                m_pages[i].Switch(false);
            }
            if (index >= 0 && index < m_pages.Length) {

                index = Mathf.Clamp(index, 0, m_pages.Length - 1);
                m_pages[index].Switch(true);
            }
        }
    }
    [System.Serializable]
    public class Page {
        public string m_named = "Unnamed";
        public GameObject[] m_toSwitch;
        public GameObject[] m_toSwitchInverse;

        public void Switch(bool value) {
            if (m_toSwitch.Length > 0)
            {
                for (int i = 0; i < m_toSwitch.Length; i++)
                {
                    if(m_toSwitch[i]!=null)
                    m_toSwitch[i].SetActive(value);
                }
            }
        }
    }
}
