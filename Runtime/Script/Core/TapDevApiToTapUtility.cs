﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TapDevApiToTapUtility : MonoBehaviour
{
    public Text m_logText;
    private ITapInput tapInputManager;
    private string connectedTapIdentifier = "";


    public void CallTapManager()
    {
        TapUtility.CallTapWithUsApplication();
    }

    void Start()
    {
        tapInputManager = TapInputManager.Instance;
        tapInputManager.OnTapInputReceived += onTapped;
        tapInputManager.OnTapConnected += onTapConnected;
        tapInputManager.OnTapDisconnected += onTapDisconnected;
        tapInputManager.OnMouseInputReceived += onMoused;
        tapInputManager.OnAirGestureInputReceived += onAirGestureInputReceived;
        tapInputManager.OnTapChangedAirGestureState += onTapChangedState;
        tapInputManager.OnRawSensorDataReceived += onRawSensorDataReceived;
        tapInputManager.EnableDebug();
        // tapInputManager.SetDefaultControllerWithMouseHIDMode(true);
        tapInputManager.SetDefaultControllerMode(true);

        //mouseHIDEnabled = false;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Screen.fullScreenMode = FullScreenMode.Windowed;
        Screen.brightness = 0;

    }


    private void Log(string text)
    {
        if (m_logText != null)
        {
            m_logText.text = string.Format("{0}\n", text) + m_logText.text;
        }
        Debug.Log(text);
    }

    void onMoused(string identifier, int vx, int vy, bool isMouse)
    {

       TU.Get(identifier).SetMouseDelta(vx, vy);
        TU.Get(identifier).SetMouseMode(isMouse);

        Log("onMoused" + identifier + ", velocity = (" + vx + "," + vy + "), isMouse " + isMouse);
    }

    void onTapped(string identifier, int combination)
    {
        bool[] arr = TapCombination.toFingers(combination);
        TapReceived received = new TapReceived(arr);
        TU.Get(identifier).AddReceived(received);

        Log("onTapped : " + identifier + ", " + combination);
    }

    void onTapConnected(string identifier, string name, int fw)
    {
        Debug.Log("onTapConnected : " + identifier + ", " + name + ", FW: " + fw);
        Log("onTapConnected : " + identifier + ", " + name);
        this.connectedTapIdentifier = identifier;
        TapUtility.CheckRegistered(identifier);
        TU.Get(identifier).SetInfo(identifier, name, fw);
        TU.Get(identifier).SetConnected(true);
    }

    void onTapDisconnected(string identifier)
    {
        Debug.Log("UNITY TAP CALLBACK --- onTapDisconnected : " + identifier);
        Log("UNITY TAP CALLBACK --- onTapDisconnected : " + identifier);
        if (identifier.Equals(this.connectedTapIdentifier))
        {
            this.connectedTapIdentifier = "";

        }
        TU.Get(identifier).SetConnected(false);
    }


    void onAirGestureInputReceived(string tapIdentifier, TapAirGesture gesture)
    {

        Log("OnAirGestureInputReceived: " + tapIdentifier + ", " + gesture.ToString());
        // NOT MANAGE YET BECAUSE I DON4T HAVE IT
    }

    void onTapChangedState(string tapIdentifier, bool isAirGesture)
    {
        Log("onTapChangedState: " + tapIdentifier + ", " + isAirGesture.ToString());
        TU.Get(tapIdentifier).SetInAirGestureMode(isAirGesture);

    }
    void onRawSensorDataReceived(string tapIdentifier, RawSensorData data)
    {
        if (data.type == RawSensorData.DataType.Device)
        {
            TU.Get(tapIdentifier).m_rawData.SetFingerAccelerometer(Finger.Thumb, data.GetPoint(RawSensorData.iDEV_THUMB));
            TU.Get(tapIdentifier).m_rawData.SetFingerAccelerometer(Finger.Index, data.GetPoint(RawSensorData.iDEV_INDEX));
            TU.Get(tapIdentifier).m_rawData.SetFingerAccelerometer(Finger.Middle, data.GetPoint(RawSensorData.iDEV_MIDDLE));
            TU.Get(tapIdentifier).m_rawData.SetFingerAccelerometer(Finger.Ring, data.GetPoint(RawSensorData.iDEV_RING));
            TU.Get(tapIdentifier).m_rawData.SetFingerAccelerometer(Finger.Pinky, data.GetPoint(RawSensorData.iDEV_PINKY));
        }
        else if (data.type == RawSensorData.DataType.IMU)
        {
            TU.Get(tapIdentifier).m_rawData.SetDeviceGryo(data.GetPoint(RawSensorData.iIMU_GYRO));
            TU.Get(tapIdentifier).m_rawData.SetDeviceAccelerometer(data.GetPoint(RawSensorData.iIMU_ACCELEROMETER));
        }
    }
}
