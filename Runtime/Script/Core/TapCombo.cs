﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class TapCombo
{
    private static TapCombo[] m_fullComboList = new TapCombo[] {
        new TapCombo("O....",'O'),
        new TapCombo(".O...",'O'),
        new TapCombo("..O..",'O'),
        new TapCombo("...O.",'O'),
        new TapCombo("....O",'O'),
        new TapCombo("...OO",'O'),
        new TapCombo("..OO.",'O'),
        new TapCombo(".OO..",'O'),
        new TapCombo("OO...",'O'),
        new TapCombo("..O.O",'O'),
        new TapCombo(".O.O.",'O'),
        new TapCombo("O.O..",'O'),
        new TapCombo(".O..O",'O'),
        new TapCombo("O..O.",'O'),
        new TapCombo("O...O",'O'),
        new TapCombo("..OOO",'O'),
        new TapCombo(".OOO.",'O'),
        new TapCombo("OOO..",'O'),
        new TapCombo(".O.OO",'O'),
        new TapCombo(".OO.O",'O'),
        new TapCombo("O..OO",'O'),
        new TapCombo("OO.O.",'O'),
        new TapCombo("O.OO.",'O'),
        new TapCombo("OO..O",'O'),
        new TapCombo("O.O.O",'O'),
        new TapCombo("OOOO.",'O'),
        new TapCombo("OOO.O",'O'),
        new TapCombo("OO.OO",'O'),
        new TapCombo("O.OOO",'O'),
        new TapCombo(".OOOO",'O'),
        new TapCombo("OOOOO",'O')
    };

    public static char IsOnDefaultChar()
    {
        return '_';
    }
    public static char IsOffDefaultChar()
    {
        return '-';
    }

    public static List<TapCombo> GetAllPossibilities() {

        return m_fullComboList.ToList();
    }


    [SerializeField] bool[] m_rawDataFingersThumbToPinky = new bool[5];
    public TapCombo(string fiveCharValue, char isOn )
    {
        if (fiveCharValue.Length != 5)
            throw new System.ArgumentException("Required string of 5 in lenght,given: "+fiveCharValue);
        
        m_rawDataFingersThumbToPinky = new bool[] {
        fiveCharValue[0]==isOn, fiveCharValue[1]==isOn, fiveCharValue[2]==isOn, fiveCharValue[3]==isOn, fiveCharValue[4]==isOn
        };
    }
    public TapCombo(bool thumb, bool index, bool middle, bool ring, bool pinky) {
        m_rawDataFingersThumbToPinky = new bool[] {
        thumb, index, middle, ring, pinky
        };
    }
    public TapCombo(int thumb, int index, int middle, int ring, int pinky) {
        m_rawDataFingersThumbToPinky = new bool[] {
        thumb!=0, index!=0, middle!=0, ring!=0, pinky!=0
        };
    }
    public TapCombo(bool[] fingersStateThumbToPinky)
    {
        for (int i = 0; i < 5; i++)
        {
            if (i < fingersStateThumbToPinky.Length)
                m_rawDataFingersThumbToPinky[i] = fingersStateThumbToPinky[i];
        }
    }

    public TapCombo()
    {
        m_rawDataFingersThumbToPinky = new bool[5];
    }

    public virtual string GetIdDescription(char isOn = '_', char isOff = '-')
    {
        string result = "";
        for (int i = 0; i < m_rawDataFingersThumbToPinky.Length; i++)
        {
            result += m_rawDataFingersThumbToPinky[i] ? isOn : isOff;
        }
        return result;
    }


    public bool IsOn(Finger fingerType)
    {
        switch (fingerType)
        {
            case Finger.Thumb: return m_rawDataFingersThumbToPinky[0];
            case Finger.Index: return m_rawDataFingersThumbToPinky[1];
            case Finger.Middle: return m_rawDataFingersThumbToPinky[2];
            case Finger.Ring: return m_rawDataFingersThumbToPinky[3];
            case Finger.Pinky: return m_rawDataFingersThumbToPinky[4];
            default:
                break;
        }
        return false;
    }

    public override bool Equals(object obj)
    {

        if (obj == null)
        {
            return false;
        }
        TapCombo tc = obj as TapCombo;
        if (tc == null)
            return false;
        for (int i = 0; i < 5; i++)
        {
            if (m_rawDataFingersThumbToPinky[i] != tc.m_rawDataFingersThumbToPinky[i])
                return false;

        }
        return true;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

}

public class HandTapCombo : TapCombo
{

    public HandTapCombo(HandSide handType, bool[] fingersState) : base(fingersState)
    {
        SetHandType(handType);
    }

    private HandSide m_handUsed;
    public void SetHandType(HandSide handType) { m_handUsed = handType; }
    public HandSide GetHandType() { return m_handUsed; }
    public bool IsLeft() { return m_handUsed == HandSide.Left; }
    public bool IsRight() { return m_handUsed == HandSide.Right; }

    public override bool Equals(object obj)
    {

        if (obj == null)
        {
            return false;
        }
        HandTapCombo htc = obj as HandTapCombo;
        if (htc == null)
            return false;
        bool isComboEqual = base.Equals(obj);
        if (isComboEqual == false)
            return false;
        if (m_handUsed != htc.m_handUsed)
            return false;
        return true;
    }

    public override int GetHashCode()
    {

        return base.GetHashCode();
    }


    public override string GetIdDescription(char isOn = '_', char isOff = '-')
    {

        return m_handUsed.ToString() + "|" + base.GetIdDescription(isOn, isOff);
    }
}