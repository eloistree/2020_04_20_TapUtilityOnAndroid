﻿
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;


public class ConnectedTap
{
    public int m_maxTapReceivedRecord = 100;

    public bool m_isStillConnected = false;
    public string m_deviceName = "";
    public string m_deviceId = "";
    public int m_tapFirmwareVersion = 0;

    public bool m_isInAirGestureMode = false;
    private int m_wantedGryoSensibility;
    private int m_wantedAccelerometerSensibility;
    private int m_wantedFingersSensibility;
    public List<TapReceived> m_received = new List<TapReceived>();

    public TapMouseData GetMouseInfo()
    {
        return m_rawMouse;
    }

    public void AddReceived(TapReceived tap)
    {
        if (m_received.Count > m_maxTapReceivedRecord)
            m_received.RemoveAt(m_maxTapReceivedRecord);
        m_received.Insert(0, tap);
        m_onTapReceived.Invoke(tap);
    }
    public TapRawData m_rawData = new TapRawData();
    public TapMouseData m_rawMouse = new TapMouseData();
    public TapMode m_currentMode = TapMode.Undefined;

    [Header("Event")]
    [SerializeField] TapReceivedMonoEvent m_onTapReceived = new TapReceivedMonoEvent();
    [System.Serializable]
    public class TapReceivedMonoEvent : UnityEvent<TapReceived> { };


    public void SetId(string identifier)
    {
        m_deviceId = identifier;
    }

    public HandSide GetHandType()
    {
        ConnectedTap l,r;
        l = TapUtility.GetLeft();
        r = TapUtility.GetRight();
        if (l!=null && l.GetIdentifiant() == GetIdentifiant())  
            return HandSide.Left;
        if (r != null && r.GetIdentifiant() == GetIdentifiant())
            return HandSide.Right;
        return HandSide.Unknown;

    }

    public void SetInfo(string identifier, string name, int fw)
    {
        m_deviceId = identifier;
        m_deviceName = name;
        m_tapFirmwareVersion = fw;
    }

    public void SetConnected(bool value)
    {
        m_isStillConnected = value;
    }
    public void SetAsMode(int value)
    {
        switch (value)
        {
            case 0: SetAsMode(0); break;
            case 1: SetAsMode(1); break;
            case 2: SetAsMode(2); break;
            case 3: SetAsMode(3); break;
            default:
                break;
        }
    }

    public void SetMouseDelta(float vx, float vy)
    {
        m_rawMouse.SetMouseDelta(vx,vy);
    }

    public void SetMouseMode(bool isOn)
    {
        m_rawMouse.SetMouseMode(isOn);
    }

  

    public void SetAsMode(TapMode mode)
    {
        m_currentMode = mode;
        switch (mode)
        {
            case TapMode.Controller:

                if(TapInputManager.Instance!=null)
                TapInputManager.Instance.StartControllerMode(m_deviceId);
                break;
            case TapMode.Text:
                if (TapInputManager.Instance != null)
                    TapInputManager.Instance.StartTextMode(m_deviceId);
                break;
            case TapMode.ControllerAndMouse:
                if (TapInputManager.Instance != null)
                    TapInputManager.Instance.StartControllerWithMouseHIDMode(m_deviceId);
                break;
            case TapMode.Sensors:
                if (TapInputManager.Instance != null)
                    TapInputManager.Instance.StartRawSensorMode(m_deviceId, m_wantedAccelerometerSensibility, m_wantedGryoSensibility, m_wantedFingersSensibility);
                break;
            case TapMode.Undefined:
                break;
            default:
                break;
        }
    }


    public void SetInAirGestureMode(bool isAirGesture)
    {
        m_isInAirGestureMode = isAirGesture;
    }

    public string GetIdentifiant()
    {
        return m_deviceId;
    }

    public void Vibrate(int milliseconds)
    {
        if (TapInputManager.Instance != null)
            TapInputManager.Instance.Vibrate(GetIdentifiant(), new int[] { milliseconds });
    }

    public bool WasMouseModeActive(DateTime from, DateTime to)
    {
      DateTime t=  m_rawMouse.GetLastMoveTime();
        return t > from && t < to;
    }
    public double GetTimeSinceLastMouseMove()
    {
        DateTime t = m_rawMouse.GetLastMoveTime();
        DateTime n = DateTime.Now;
        return (n - t).TotalSeconds;
    }

    public void SetSensorsSensibility(int acc, int gyro, int finger)
    {
        m_wantedAccelerometerSensibility = Mathf.Clamp(acc, 0, 4);
        m_wantedGryoSensibility = Mathf.Clamp(gyro, 0, 4);
        m_wantedFingersSensibility = Mathf.Clamp(finger, 0, 5);
    }

    public string GetName()
    {
        return m_deviceName;
    }

    public bool IsConnected()
    {
        return m_isStillConnected;
    }

    public int GetFirmwareId()
    {
        return m_tapFirmwareVersion;
    }

    public bool IsMouseLaserSensorWasActive()
    {
        return m_rawMouse.IsMouseLaserSensorWasActive();
    }

    public Vector3 GetGyro()
    {
        return m_rawData.GetGyro();
    }

    public Vector3 GetFingerAccelerometer(int fingerIndexThumbToPinky)
    {
        switch (fingerIndexThumbToPinky)
        {
            case 0: return GetFingerAccelerometer(Finger.Thumb);
            case 1: return GetFingerAccelerometer(Finger.Index);
            case 2: return GetFingerAccelerometer(Finger.Middle);
            case 3: return GetFingerAccelerometer(Finger.Ring);
            case 4: return GetFingerAccelerometer(Finger.Pinky);
            default:
                return Vector3.zero;
        }
    }
    public Vector3 GetFingerAccelerometer(Finger finger)
    {
        return m_rawData.GetFingerAccelerometer(finger);
    }
    public Vector3 GetAccelerometer()
    {
        return m_rawData.GetAccelerometer();
    }

    public Vector3 GetMouseVelocity()

    {
        return m_rawMouse.GetVelocity();
    }

    public string GetHistoryAsMultiLineText(string on, string off)
    {
        string description = "";
        for (int i = 0; i < m_received.Count; i++)
        {
            description += m_received[i].GetDescription() + "\n";
        }
        return description;
    }
    public string GetHistoryAsMultiLineText(char on, char off)
    {
        return GetHistoryAsMultiLineText("" + on, "" + off);
    }

    public TapReceived GetLastTapReceived()
    {
        if (m_received.Count < 1)
            return new TapReceived();
        return m_received[0];
    }


    public bool WasTap(Finger finger)
    {
        return GetLastTapReceived().IsOn(finger);
    }

    public bool WasTapSince(DateTime fromExplusif, DateTime toInclusif, out TapReceived firstFound, out TapReceived[] found)
    {
        found = m_received.Where(k => k.GetWhen() > fromExplusif && k.GetWhen() <= toInclusif).ToArray();
        if (found.Length > 0)
        {
            firstFound = found[0];
            return true;
        }

        firstFound = null;
        return false;
    }
}

public class TapRawData
{
    Vector3 m_thumb = Vector3.zero, index = Vector3.zero, middle = Vector3.zero, ring = Vector3.zero, pinky = Vector3.zero;
    Vector3 m_deviceAdditionThumbAccelerometer = Vector3.zero;
    Vector3 m_deviceAdditionGryo = Vector3.zero;

    public Vector3 GetGyro() { return m_deviceAdditionGryo; }
    public Vector3 GetAccelerometer() { return m_deviceAdditionThumbAccelerometer; }
    public Vector3 GetFingerAccelerometer(Finger fingerType)
    {
        switch (fingerType)
        {
            case Finger.Thumb: return m_thumb;
            case Finger.Index: return index;
            case Finger.Middle: return middle;
            case Finger.Ring: return ring;
            case Finger.Pinky: return pinky;
            default: return Vector3.zero;
        }
    }

    internal void SetDeviceGryo(Vector3 value)
    {
        if (value != null)
            m_deviceAdditionGryo = value;
    }

    internal void SetDeviceAccelerometer(Vector3 value)
    {
        if (value != null)
            m_deviceAdditionThumbAccelerometer = value;
    }

    internal void SetFingerAccelerometer(Finger finger, Vector3 value)
    {
        if (value != null)
        {
            switch (finger)
            {
                case Finger.Thumb:
                    m_thumb = value;
                    break;
                case Finger.Index:
                    index = value;
                    break;
                case Finger.Middle:
                    middle = value;
                    break;
                case Finger.Ring:
                    ring = value;
                    break;
                case Finger.Pinky:
                    pinky = value;
                    break;
                default:
                    break;
            }

        }
    }
}
public class TapMouseData
{
    private Vector2 m_rawData = Vector2.zero;
    private bool m_rawIsMouse = false;
    private DateTime m_lastMouseModeSwitch = DateTime.Now;
    private DateTime m_lastMouseMove = DateTime.Now;
    public int m_maxRecord=1000;
    public List<MouseDeltaRecord> m_mouseMoves = new List<MouseDeltaRecord>();

   
    public Vector2 GetVelocity()
    {
        return m_rawData;
    }

    public bool IsMouseLaserSensorWasActive()
    {
        return m_rawIsMouse;
    }

    public DateTime GetLastMoveTime()
    {
        return m_lastMouseMove;
    }

    public void SetMouseDelta(float vx, float vy,bool record=true)
    {
        m_lastMouseMove = DateTime.Now;
        m_rawData.x = vx;
        m_rawData.y = vy;

        if (record) { 
            m_mouseMoves.Insert(0, new MouseDeltaRecord(vx, vy));
            if (m_mouseMoves.Count >= m_maxRecord )
                m_mouseMoves.RemoveAt(m_mouseMoves.Count - 1);
        }

    }

    public MouseDeltaRecord [] GetDelta(DateTime fromInclusif, DateTime toExclusif)
    {
        return m_mouseMoves.Where(k => k.m_when>= fromInclusif && k.m_when< toExclusif).OrderByDescending(k=>k.m_when).ToArray();
    }

    public void SetMouseMode(bool isOn)
    {
        if (m_rawIsMouse != isOn) { 
            m_lastMouseModeSwitch = DateTime.Now;
            m_rawIsMouse = isOn;
        }
    }

    public int GetMouseRecordStore() { return m_mouseMoves.Count; }
    public MouseDeltaRecord GetLastMove()
    {
        if (m_mouseMoves.Count > 0)
            return m_mouseMoves[0];
        return null;
    }
}
public class MouseDeltaRecord
{
    public float m_x;
    public float m_y;
    public DateTime m_when;

    public MouseDeltaRecord(float vx, float vy)
    {
        m_x = vx;
        m_y = vy;
        m_when = DateTime.Now;
    }

    public DateTime GetTime() { return m_when; }
    public float GetMoveX() { return m_x; }
    public float GetMoveY() { return m_y; }

}
public class TapReceived
{
    public TapCombo m_tapInfo= new TapCombo();
    public DateTime m_whenTapped;

    public TapReceived()
    {
        m_tapInfo = new TapCombo( new bool[5]);
        m_whenTapped = DateTime.Now;
    }

    public TapReceived(bool[] rawData)
    {
        m_tapInfo = new TapCombo(rawData);
        m_whenTapped = DateTime.Now;
    }

    public void SetTimeTo(DateTime time)
    {
        m_whenTapped = time;
    }
    public DateTime GetWhen()
    {
        return m_whenTapped;
    }

    public string GetDescription(char on='_', char off = '-')
    {
        return m_tapInfo.GetIdDescription(on,off);
    }

    public bool IsOn(Finger finger)
    {
        return m_tapInfo.IsOn(finger);
    }

    public TapCombo GetValue()
    {
        return m_tapInfo;
    }
}
public enum Finger : int { Thumb, Index, Middle, Ring, Pinky }
public enum HandSide { Left, Right, Unknown }
public enum TapMode { Controller, Text, ControllerAndMouse, Sensors,
    Undefined
}