﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class TapUtilityMono : MonoBehaviour
{

    public void Awake()
    {

        TapUtility.LoadHandsIdFromDataFolder();
    }

    private void OnDestroy()
    {

        TapUtility.SaveHandsIdToDataFolder();
    }

    public void SetListenerOfLog(bool on) {

        TapUtility.SetListenerOfLog(on);
    }

    public void OpenTapManager() {
        TapUtility.CallTapWithUsApplication();
    }

    public void SwitchLeftAndRight() {
        TapUtility.SwitchLeftAndRight();
    }

    public void SetLeftHandId(string id)
    {
        TapUtility.SetLeftHand(id, true);
    }
    public void SetLeftRightId(string id)
    {
        TapUtility.SetRightHand(id, true);
    }
    public void SaveHandIdInDataFolder()
    {
        TapUtility.SaveHandsIdToDataFolder();
    }
    public void LoadHandIdFromDataFolder()
    {
        TapUtility.LoadHandsIdFromDataFolder();
    }

  

    public void Test (){ 
    
     
    }
}
