﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class TapMappingStringRegisterToJson {


    public static string GetRegisterAsJSON(TapMappingRegister<string> tapRegister)
    {

        return JsonUtility.ToJson(GetTapMappingRegisterSave(tapRegister));
    }
    public static TapMappingRegister<string> GetRegisterFromJSON(string tapRegisterJson, out TapMappingRegisterAsStringSave recovered)
    {
        recovered =JsonUtility.FromJson <TapMappingRegisterAsStringSave>(tapRegisterJson);
        return GetRegisterFromSave(recovered);
    }

    public static TapMappingRegister<string> GetRegisterFromSave(TapMappingRegisterAsStringSave save) {

        TapMappingRegister<string> result = new TapMappingRegister<string>();

        for (int i = 0; i < save.m_mapping.Count; i++)
        {
            TapMapping<string> map = new TapMapping<string>(save.m_mapping[i].m_name);
            for (int j = 0; j < save.m_mapping[i].m_keyToValue.Count; j++)
            {
                string id = save.m_mapping[i].m_keyToValue[j].id;
                string text = save.m_mapping[i].m_keyToValue[j].value;

                map.Set(new TapCombo(id,TapCombo.IsOnDefaultChar()), text);
            }
            result.Add(map);
        }
        return result;

    }


    public static TapMappingRegisterAsStringSave GetTapMappingRegisterSave( TapMappingRegister<string> tapRegister) {
        TapMappingRegisterAsStringSave result = new TapMappingRegisterAsStringSave();

        List<string> maps =  tapRegister.GetMappingRegistered();
        Debug.Log("Maps: " + maps.Count);
        for (int i = 0; i < maps.Count; i++)
        {
            result.m_mapping.Add(GetTapMappingSave(tapRegister.GetMapping(maps[i])));
        }
        return result; 
    }

    public static  TapMappingAsStringSave GetTapMappingSave( TapMapping<string> tapMap) {
        TapMappingAsStringSave result = new TapMappingAsStringSave();
        result.m_name = tapMap.GetIdName();

        List<TapCombo> possibilites = TapCombo.GetAllPossibilities();
        for (int i = 0; i < possibilites.Count; i++)
        {
            string id = possibilites[i].GetIdDescription(TapCombo.IsOnDefaultChar(), TapCombo.IsOffDefaultChar());
            string text = "";
            if(tapMap.IsValueRegistered(possibilites[i]))
                text=tapMap.GetValue(possibilites[i]);
            result.m_keyToValue.Add(new IdToText(id,text));
        }
        return result;
    
    }
}
[System.Serializable]
public class TapMappingRegisterAsStringSave{
    public List<TapMappingAsStringSave> m_mapping = new List<TapMappingAsStringSave>();
}
[System.Serializable]
public class TapMappingAsStringSave {
    public string m_name="";
    public List<IdToText> m_keyToValue = new List<IdToText>();
}
[System.Serializable]
public class IdToText {
    public string id, value;

    public IdToText(string id, string text)
    {
        this.id = id;
        this.value = text;
    }
}


public class TapMappingRegister<T> {
    Dictionary<string, TapMapping<T>> m_mapping = new Dictionary<string, TapMapping<T>>();

    public void Add(TapMapping<T> mapping) {
        if (!m_mapping.ContainsKey(mapping.GetIdName()))
            m_mapping.Add(mapping.GetIdName(), mapping);
        else m_mapping[mapping.GetIdName()] = mapping;
    }
    public  List<string> GetMappingRegistered() {

        return m_mapping.Keys.ToList();
    }

    public TapMapping<T> GetMapping(string id) {
        if (!m_mapping.ContainsKey(id))
            return null;
        else 
            return m_mapping[id];
    }

    internal bool IsRegistered(string mapId)
    {
        return m_mapping.ContainsKey(mapId);
    }

    internal void AddNew(string mapId)
    {
        Add(new TapMapping<T>(mapId));
    }
}

public class TapMapping<T> {
    public TapMapping(string mappingName) {
        m_name = mappingName;
    }
    private string m_name="";
    private Dictionary<string, T> m_comboToValue = new Dictionary<string, T>();
    public string GetIdName() { return m_name; }

    public bool IsValueRegistered(TapCombo combo) {
       return m_comboToValue.ContainsKey(GetIdOf(combo));
    }
    public T GetValue(TapCombo combo) {
        return m_comboToValue[GetIdOf(combo)];
    }

    public void Set(TapCombo handComboId, T value)
    {
        string id = GetIdOf(handComboId);
        if (string.IsNullOrEmpty(id))
            return;
        AddOrSetValue(value, id);
    }

    private void AddOrSetValue(T value, string id)
    {
        if (!m_comboToValue.ContainsKey(id))
            m_comboToValue.Add(id, value);
        else
            m_comboToValue[id] = value;
    }

   
    public string GetIdOf(TapCombo handCombo)
    {
        if (handCombo != null)
            return handCombo.GetIdDescription('O', '_');
        return "";
    }

}

