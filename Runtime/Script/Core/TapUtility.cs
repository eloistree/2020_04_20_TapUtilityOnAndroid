﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TapUtility
{

    public delegate void OnHandsChangeID(string leftHandId, string rightHandID);

    private static OnHandsChangeID m_onHandChangeId;
    public static List<ConnectedTap> m_devices = new List<ConnectedTap>();
    public static ConnectedTap GetById(string id)
    {
        for (int i = 0; i < m_devices.Count; i++)
        {
            if (id == m_devices[i].GetIdentifiant())
                return m_devices[i];
        }
        return null;
    }

    public static ConnectedTap GetHandOrFirstFound(HandSide handtype)
    {
        if (m_devices.Count <= 0) return null;
        if (handtype == HandSide.Left) return GetLeft();
        if (handtype == HandSide.Right) return GetRight();
        return m_devices[0];
    }

    internal static ConnectedTap GetHandOrNull(HandSide handtype)
    {
        if (m_devices.Count <= 0) return null;
        if (handtype == HandSide.Left) return GetLeft();
        if (handtype == HandSide.Right) return GetRight();
        return null;
    }

    internal static TapReceived GetLastTapReceived(out ConnectedTap device)
    {
        device = null;
        TapReceived last = null;
        TapReceived tmp = null;
        for (int i = 0; i < m_devices.Count; i++)
        {
            if (last == null)
            {
                last = m_devices[i].GetLastTapReceived();
                device = m_devices[i];
            }
            else
            {

                tmp = m_devices[i].GetLastTapReceived();
                if (tmp != null && tmp.GetWhen() > last.GetWhen())
                {
                    last = tmp;
                    device = m_devices[i];
                }
            }

        }
        return last;

    }
    public static List<TapReceived> GetLastTapReceivedFromTo(DateTime from, DateTime to)
    {
        List<TapReceived> allTap = new List<TapReceived>();
        TapReceived[] found = null;
        TapReceived last=null;

        for (int i = 0; i < m_devices.Count; i++)
        {
            if(m_devices[i].WasTapSince(from, to, out last, out found))
                allTap.AddRange (found);

        }
        return allTap;

    }

    public static void AddListenerOnLeftRightHandChanged(OnHandsChangeID listener)
    {
            m_onHandChangeId += listener;
    }

    public static string GetStoredLeftHandId()
    {
        return m_leftId;
    }
    public static string GetStoredRightHandId()
    {
        return m_rightId;
    }

    public static void RemoveListenerOnLeftRightHandChanged(OnHandsChangeID listener)
    {
        m_onHandChangeId -= listener;
    }


    public static void SwitchLeftAndRight()
    {
        string tmp = m_leftId;
        m_leftId = m_rightId ;
        m_rightId = tmp;
        NotifyHandsIdChanged();
    }


    public static string GetLeftRightHandInfo(bool saveOnharddrive = true) {

        return UnityDirectoryStorage.LoadFile("TapUtility", "LeftRight.json", saveOnharddrive);
    }

    public static void SaveHandsIdToDataFolder(bool saveOnharddrive=true)
    {

         UnityDirectoryStorage.SaveFile("TapUtility", "LeftRight.json", m_leftId + "#" + m_rightId , saveOnharddrive);
    }

    public static void LoadHandsIdFromDataFolder()
    {
      
        string txt = GetLeftRightHandInfo();
        if (string.IsNullOrEmpty(txt))
            return;
        string [] tokens = txt.Split('#');
        if (tokens.Length == 2)
        {
            m_leftId = tokens[0];
            m_rightId = tokens[1];
            NotifyHandsIdChanged();
        }
    }

    public static void SetLeftHand(string idOrIndex, bool withOverride = true)
    {
        SetHandWithIdOrIndex(ref m_leftId, idOrIndex, withOverride);

    }
    public static void SetRightHand(string idOrIndex, bool withOverride = true)
    {
        SetHandWithIdOrIndex(ref m_rightId, idOrIndex, withOverride);
    }
    private static void SetHandWithIdOrIndex(ref string handIdStorage, string idOrIndex, bool withOverride)
    {
        if (idOrIndex == null || idOrIndex == "")
            return;

        if (idOrIndex.Length < 3)
        {
            idOrIndex = GetIdFromIndex(idOrIndex);
        }

        if (withOverride)
        {
            handIdStorage = idOrIndex;
            NotifyHandsIdChanged();

        }
        else if (m_leftId == null || m_leftId.Length > 0)
        {
            handIdStorage = idOrIndex;
            NotifyHandsIdChanged();
        }
    }

    private static string GetIdFromIndex(string idOrIndex)
    {
        int index = -1;
        if (int.TryParse(idOrIndex, out index))
        {
            if (index < GetRegisteredCount())
                idOrIndex = GetByConnectionOrder(index).GetIdentifiant();
        }

        return idOrIndex;
    }

    private static void NotifyHandsIdChanged()
    {
        if (m_onHandChangeId != null)
            m_onHandChangeId.Invoke(m_leftId, m_rightId);
    }

   

    public static ConnectedTap GetFirstConnected() { return m_devices.Count <= 0 ? null : m_devices[0]; }
    public static ConnectedTap[] GetAllConnected() { return m_devices.ToArray(); }
    public static int GetRegisteredCount() { return m_devices.Count; }

    public static ConnectedTap GetByConnectionOrder(int index)
    {
        if (index < 0 || index >= m_devices.Count)
            return null;
        return m_devices[index];
    }

    public static void CheckRegistered(string id)
    {
        if (string.IsNullOrEmpty(id)) return;
        ConnectedTap tap = GetById(id);
        if (tap == null)
        {
            tap = new ConnectedTap();
            tap.SetId(id);
            m_devices.Add(tap);
        }
    }

    static string m_leftId;
    static string m_rightId;
    public static ConnectedTap GetLeft()
    {
        return GetById(m_leftId);

    }

    public static ConnectedTap GetRight()
    {
        return GetById(m_rightId);
    }

    public static void CallTapWithUsApplication()
    {
#if UNITY_EDITOR
   
        Application.OpenURL("https://play.google.com/store/apps/details?id=tapwithus.com.tapmanager&hl=en");

#elif UNITY_ANDROID
             string bundleId = "tapwithus.com.tapmanager"; // your target bundle id
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

        //if the app is installed, no errors. Else, doesn't get past next line
        AndroidJavaObject launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleId);

        ca.Call("startActivity", launchIntent);

        up.Dispose();
        ca.Dispose();
        packageManager.Dispose();
        launchIntent.Dispose();
#endif

    }

    public static void SetListenerOfLog(bool on)
    {
        if(on)
            TapInputManager.Instance.EnableDebug();
        else
            TapInputManager.Instance.DisableDebug();
    }
}

public class TU
{
    public static ConnectedTap Get(string id) { return TapUtility.GetById(id); }
    public static ConnectedTap Get(int orderIndex) { return TapUtility.GetByConnectionOrder(orderIndex); }

}
