﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TurnOfRendering_OnDoubleFingerTouch : MonoBehaviour
{
    public int m_touchCount;
    public int m_touchCountPreviously;
    public bool m_switchState=true;
    public bool m_useLeftRightClickMouseToSimulate=true;
    public SwitchBooleanEvent m_requredSwitch;
    [System.Serializable]
    public class SwitchBooleanEvent : UnityEvent<bool>{}
    void Update()
    {

        if (Input.GetMouseButton(1) && Input.GetMouseButtonDown(0))
        {
            m_switchState = !m_switchState;
            m_requredSwitch.Invoke(m_switchState);
        }

        m_touchCount = Input.touchCount;
        if (m_touchCount != m_touchCountPreviously) { 
        
            if(m_touchCountPreviously<3 && m_touchCount >= 3)
            {
                m_switchState =! m_switchState;
                m_requredSwitch.Invoke(m_switchState);

            }
        
        }
        m_touchCountPreviously = m_touchCount;
        
    }
}
